"""
A client that checks garage health for a supplied garage_id, date and time.
It downloads latest data and checks for anomalies and reports whether that
last obtained data value in real time is anomalous or not
"""
from real_time_anomaly import check_garage_health, set_home_dimension
import sys

metrics = ["occupancy", "revenue", "transactions"]

if __name__ == '__main__':

    if len(sys.argv) < 4:
        print("Usage {} garage_id date(yyyy-mm-dd) hour".format(sys.argv[0]))
        sys.exit(0)

    set_home_dimension(sys.argv[1])
    for metric in metrics:
        check_garage_health(sys.argv[1], sys.argv[2], sys.argv[3], metric)