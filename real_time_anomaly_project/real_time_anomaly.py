"""
Python library for checking if the real time monitored data is a outlier.
Apr 13: Checking for outliers only if the current value is zero
TODO: Deal with non-zero velues for real-time-data as well
"""

import json
import subprocess
import os
import sys
import datetime
import numpy as np
import smirnov_grubbs as grubbs

home_dimension = ""
date_format = "%Y-%m-%d"
days_in_the_past = 60
holidays_json_file = "./holidays.json"
holidays = []

garage_name = ""


def get_json_info_using_curl(url):
    """
    Download json string using curl

    :param str url: url to download
    :return: str
    """
    token = str(os.environ['Bearer'])
    if token is None:
        print("Bearer not set")
        sys.exit(0)

    url_str = 'curl -s -i "' + url + '" -H "Authorization: Bearer ' + token + '"'

    output = subprocess.check_output(url_str, shell=True)
    res = output.decode("utf-8").splitlines()
    if len(res) < 10:
        print("bad result from url", url)
        sys.exit(0)
    return output.decode("utf-8").splitlines()[9]


def get_garage_metadata(garage_id):
    """
    Get the garage metadata and return json string

    :param str garage_id: the garage ID
    :return: str
    """
    # treat special garages
    if garage_id == '552026':
        garage_url = 'unico__g552026'
    elif garage_id == '516873':
        garage_url = 'premier__sondock'
    elif garage_id == '780376':
        garage_url = '780376'
    else:
        garage_url = 'c__g' + garage_id
    url = "https://my.smarking.net/api/users/v1/garages/" + garage_url
    garage_metadata = get_json_info_using_curl(url)
    return garage_metadata


def read_holidays_json():
    """
    Creates a list of holiday dates

    :return: list[str]
    """
    global holidays

    with open(holidays_json_file, 'r') as f:
        info = json.load(f)
        for i in info:
            date = i['start'][0:4] + "-" + i['start'][4:6] + "-" + i['start'][6:8]
            holidays.append(date)
    return holidays


def set_home_dimension(garage_id):
    """
    set the garage home dimension to make the url

    :param str garage_id: garage ID
    :return: None
    """

    global home_dimension, garage_name

    garage_metadata = json.loads(get_garage_metadata(garage_id))

    if garage_metadata is None:
        home_dimension = ""
    else:
        # extract the home dimension
        home_dimension = garage_metadata["homeDimension"].replace(" ", "+")
        garage_name = garage_metadata["name"]


def get_info_dict(str_info):
    """
    This function creates a dictionary out of JSON object.
    keys are groups (Contract/ Transient) etc.

    :param str str_info: Input json string
    :return: dict
    """
    dictionary = dict()
    info = json.loads(str_info)

    for item in info['value']:
        # check if value contains anything
        group = item.get("group").encode('ascii', 'ignore')
        dictionary[group] = np.array(item.get("value"))
    return dictionary


def get_data(garage_id, date, time, metric):
    """
    This function generates a dictionary for each group for the given
    metric.  key is the group (contract, transient) and values are
    the values until the given time of the day

    :param garage_id: str
    :param date: str
    :param time: str
    :param metric: str
    :return: dict
    """
    global days_in_the_past

    # get data for the past days_in_the_past days
    date_given = datetime.datetime.strptime(date, date_format)
    start_date = date_given - datetime.timedelta(days_in_the_past)

    # calculate number of data_points(hours) to download
    hours = days_in_the_past * 24 + 1 + int(time)
    day_str = str(start_date.year) + "-" + str(start_date.month).zfill(2) \
              + "-" + str(start_date.day).zfill(2)

    url = "https://my.smarking.net/api/ds/v3/garages/" \
          + garage_id + "/past/" + metric + "/from/" + day_str + \
          "T00:00:00/" + str(hours) + "/1h?gb=" + home_dimension

    return get_info_dict(get_json_info_using_curl(url))


def get_outliers_grubbs(array):
    """
    Return if the last value of a given array is a outlier

    :param list array: the given array
    :return: bool
    """

    # if not enough data-points, return false
    if len(array) < 10:
        return False
    # alpha value is found after experimentation
    outliers = grubbs.test(array, alpha=0.05)
    return array[len(array) - 1] in outliers


def get_zero_strip_lengths(array):
    """
    Takes an array and returns an array of zero strip
    lengths in that array.
    e.g input -> [0, 0, 1, 2, 0, 0, 0]
        output -> [2, 3]
    :param list array: given array
    :return: list
    """
    zero_strip_lengths = []
    count = 0
    for i in range(len(array)):
        if array[i] == 0:
            count += 1
        else:
            if count == 0:
                continue
            else:
                zero_strip_lengths.append(count)
                count = 0
    if count != 0:
        # we got zeros at the end
        zero_strip_lengths.append(count)
    return zero_strip_lengths


def check_for_outlier(array):
    """
    This function checks the number of all-zero strips
    in a given array and returns whether the length
    of the last zero-strip is an outlier among all
    lengths of zero-strips

    e.g: [0,0,1,2,0,3,3,0,0,0,0,0] --> True
         [0,0,0,3,3,0,0,0] --> False

    :param list array: given array
    :return: bool
    """

    #sanity check
    if len(array) == 0:
        return False

    # if the last data point is non-zero, does not bother
    if array[len(array) -1] != 0:
        return False

    # if mostly historical data is 0, no anomaly
    zero_fraction = ((len(array) - np.count_nonzero(array)) / len(array)) * 100

    # found through experiments
    if zero_fraction > 85:
        return False

    zero_strip_lengths=get_zero_strip_lengths(array)

    # if just one zero strip count, means we got all zeros
    if len(zero_strip_lengths) == 1:
        return False
    # if zero-strips are rare (less than 5%), raise an anomaly
    elif len(zero_strip_lengths) < 5:
        return True
    else:
        return get_outliers_grubbs(zero_strip_lengths)


def output_url(garage_id, date, time, metric, key):
    """
    Generates and prints Info string with URL so that anomalies can be verified
    Can be easily modified to form a pandas dataframe

    :param str garage_id:
    :param str date:
    :param str time:
    :param str metric:
    :param str key:
    :return: None
    """
    # get data for the past days_in_the_past days
    date_given = datetime.datetime.strptime(date, date_format)
    start_date = date_given - datetime.timedelta(12)
    end_date = date_given + datetime.timedelta(1)

    day_str = str(start_date.year) + "-" + str(start_date.month).zfill(2) \
              + "-" + str(start_date.day).zfill(2)
    to_day_str = str(end_date.year) + "-" + str(end_date.month).zfill(2) \
              + "-" + str(end_date.day).zfill(2)

    url = "https://my.smarking.net/c/g"+garage_id+"/"+metric+"?fromDateStr=" + \
          day_str+"&toDateStr="+to_day_str+"&granularity=Hourly"

    print("{}, {}, {} , {}, {}, {}".format(garage_name, garage_id, date, time,
                                                       url, key))


def get_weekend_indices(date):
    """
    Get indices of weekends in the past so that
    their data can be extracted from the whole blob
    of downloaded data

    :param str date: date
    :return: list
    """

    global days_in_the_past
    weekend_indices = []
    date_temp = datetime.datetime.strptime(date, date_format)

    for i in range(1, days_in_the_past):
        if (date_temp - datetime.timedelta(i)).weekday() in [5, 6]:
            weekend_indices.append(i)
    return weekend_indices


def get_holiday_indices(date):
    """
        Get indices of holidays in the past so that
        their data can be extracted from the whole blob
        of downloaded data

        :param str date: date
        :return: list
    """

    global days_in_the_past, holidays
    holiday_indices = []
    date_temp = datetime.datetime.strptime(date, date_format)

    for i in range(1, days_in_the_past):
        date_str = str((date_temp - datetime.timedelta(i)).year) + "-"\
                   + str((date_temp - datetime.timedelta(i)).month).zfill(2) + "-"\
                   + str((date_temp - datetime.timedelta(i)).day).zfill(2)
        if date_str in holidays:
            holiday_indices.append(i)

    return holiday_indices


def modify_weekends(data_dict, date, hour):
    """
    This function takes a historical data dictionary and filters
    the dictionary to keep only weekend data

    :param data_dict: dict
    :param date: date
    :return: None
    """

    weekend_indices_in_history = get_weekend_indices(date)

    # modify the dictionary with only weekend data
    for key in data_dict.keys():
        temp_values = []
        for jj in reversed(weekend_indices_in_history):
            for k in data_dict[key]\
                    [(days_in_the_past - jj) * 24:((days_in_the_past - jj) * 24) + 24]:
                temp_values.append(k)
        # add data for the current day
        for k in data_dict[key]\
                [days_in_the_past * 24:(days_in_the_past * 24) + int(hour) + 1]:
            temp_values.append(k)
        data_dict[key] = temp_values


def modify_holidays(data_dict, date, hour):
    """
        This function takes a historical data dictionary and filters
        the dictionary to keep only holiday data
    """

    holiday_indices_in_history = get_holiday_indices(date)

    # modify the dictionary with only holiday data
    for key in data_dict.keys():
        temp_values = []
        for jj in reversed(holiday_indices_in_history):
            for k in data_dict[key]\
                    [(days_in_the_past - jj) * 24:((days_in_the_past - jj) * 24) + 24]:
                temp_values.append(k)
        # add data for the current day
        for k in data_dict[key]\
                    [days_in_the_past * 24:(days_in_the_past * 24) + int(hour) + 1]:
            temp_values.append(k)
        data_dict[key] = temp_values


def check_garage_health(garage_id, date, time, metric):
    """
    Monitors real time data for a given garage, date, time and metric
    and prints out string if anomalies are detected

    """
    # get data for the day from beginning till this hour (given by time)
    short_term_data_dictionary = get_data(garage_id, date, time, metric)

    global holidays
    holidays = read_holidays_json()

    # treat weekends specially
    day_of_the_week = datetime.datetime.strptime(date, date_format).weekday()
    if day_of_the_week in [5, 6]:
        modify_weekends(short_term_data_dictionary, date, time)
    # treat holidays specially, if a holiday is a weekend,
    # being weekend is given priority
    elif date in holidays:
        modify_holidays(short_term_data_dictionary, date, time)

    for key in short_term_data_dictionary.keys():
        if check_for_outlier(short_term_data_dictionary[key]):
            output_url(garage_id, date, time, metric, key)
