'''
This file is for the smarking project on measuring accuracy of rolling data.
Some garages send transactions later for open tickets.
We want to measure how much modification is done and how that affects prediction.
Smarking produces occupancy not only based on raw data but based on previous distribution
'''
import json, os, sys, datetime, subprocess, time

home_dimension = ""
date_format = "%Y-%m-%d"

metrics = ["occupancy", "entries", "exits"]


def get_json_info_using_curl(url:str)->str:
    """
    Download json string using curl

    :param url: str
    :return: str
    """
    token = str(os.environ['Bearer'])
    if token is None:
        print("Bearer not set")
        sys.exit(0)

    url_str = 'curl -s -i "' + url + '" -H "Authorization: Bearer ' + token + '"'
    # print(url_str)
    try:
        output = subprocess.check_output(url_str, shell=True, timeout=20)
    except subprocess.TimeoutExpired:
        return None
    res = output.decode("utf-8").splitlines()
    if len(res) < 10:
        print("bad result from url", url)
        return None
    return output.decode("utf-8").splitlines()[9]

def get_garage_metadata(garage_id:str)->str:
    """
    Get the garage metadata and return

    :param garage_id: str
    :return: str
    """
    # treat special garages
    if garage_id == '552026':
        garage_url = 'unico__g552026'
    elif garage_id == '516873':
        garage_url = 'premier__sondock'
    elif garage_id == '780376':
        garage_url = '780376'
    else:
        garage_url = 'c__g' + garage_id
    url = "https://my.smarking.net/api/users/v1/garages/" + garage_url
    garage_metadata = get_json_info_using_curl(url)
    return garage_metadata


def set_home_dimension(garage_id:str)->None:
    """
    set the garage home dimension to make the url

    :param garage_id: str
    :return: None
    """

    global home_dimension, garage_name

    garage_metadata = get_garage_metadata(garage_id)
    if garage_metadata is None:
        return None
    garage_metadata = json.loads(garage_metadata)

    # print(garage_metadata)
    if garage_metadata is None:
        home_dimension = ""
    else:
        # extract the home dimension
        home_dimension = garage_metadata["homeDimension"].replace(" ", "+")
        garage_name = garage_metadata["name"]

    return 1


def download_metric(garage_id, metric):
    # we start downloading data from April 1 2018 until 3 days in future
    # so that we can later see the effect for both short and long term forecast
    # and we do this each hour
    start_date = datetime.datetime.strptime("2018-04-01", date_format)
    current_date = datetime.datetime.now()

    hours = ((current_date - start_date).days + 3) * 24

    url = "https://my.smarking.net/api/ds/v3/garages/" \
          + str(garage_id) + "/past/" + metric + "/from/2018-04-01T00:00:00/"\
          +str(hours)+"/1h?gb=" + home_dimension
    # print(url)
    info = get_json_info_using_curl(url)

    # print("Info {}".format(info))

    time_now = datetime.datetime.now()
    file_time_string = str(time_now.year)+"-"+str(time_now.month) + "-" + str(time_now.day) \
                       + "-" + str(time_now.hour) + "-" + str(time_now.minute)
    file_name = "./data/"+str(garage_id) + "_" + str(metric) + "_" + file_time_string

    # print(file_name)

    with open(file_name, "w") as text_file:
        text_file.write("{}".format(info))


def download_data_indiv(garage_id):
    if set_home_dimension(garage_id) is None:
        print("No metadata for {}".format(garage_id))
        return None

    for metric in metrics:
        download_metric(garage_id, metric)



def download_data():
    with open('garages') as f:
        garage_ids = f.readlines()
    garage_ids = [i.strip() for i in garage_ids]
    for id in garage_ids:
        download_data_indiv(id)

if __name__ == '__main__':
    while True:
        download_data()
        time.sleep(3600)