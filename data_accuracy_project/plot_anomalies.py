#this file reads a result file genrated by 
# historical anomaly scripts and produces a histogram from the file

from matplotlib import pyplot as plt
import sys
import numpy as np



def plot_hist(file_name):
    with open(file_name) as f:
        lines =  f.readlines()

    values = np.array([float(i.split(',')[4].strip()) for i in lines])
    # print(max(values))
    fig1 = plt.figure()
    # plt.xticks(np.arange(min(values), max(values) + 1, 2000.0), rotation='vertical', fontsize=6)
    # axes = plt.gca()
    # axes.set_ylim([0, 200])
    plt.hist(values, bins=10)
    fig1.savefig(file_name + '.png')

if __name__ == '__main__':

    plot_hist(sys.argv[1])
