"""
This file takes a data and given hour and measures how the data for that given date and time changes (evolves) in the future

"""
import sys
import os
import datetime
import json

groups = ["occupancy", "entries", "exits"]
date_format = "%Y-%m-%d-%H-%M"

def filter_names(file_names, date, time):
    filtered = []
    for f in file_names:
        date_f = f.split("_")[2]
        if datetime.datetime.strptime(date_f, date_format) > datetime.datetime.strptime(date+"-"+time+"-00", date_format):
            filtered.append(f)

    return filtered

def make_dict_from_json(info):
    dictionary = {}
    json_info = json.loads(info)
    for item in json_info['value']:
        # check if value contains anything
        group = item.get("group").encode('ascii', 'ignore')
        dictionary[group] = item.get("value")
    return dictionary

def make_dictionary(garage_id, date, time):
    """
    makes a dictionary of downloaded data for files with date >= given date
    :param garage_id:
    :param date:
    :param time:
    :return:
    """
    file_names = [fn for fn in os.listdir("./data") if garage_id in fn and "occupancy" in fn]

    # filter based on date
    filtered_file_names = filter_names(file_names, date, time)

    dictionary =  {}
    for file in filtered_file_names:
        # print(file)
        with open("./data/"+file) as f:
            content = f.readlines()
            dictionary[file.split("_")[2]] = make_dict_from_json(content[0])

    return dictionary

def print_changes(garage_id, values, key_inside, date, time):
    """
    Measures changes over time and reports the places when there is a change
    :param values:
    :param key_inside:
    :param date:
    :param time:
    :param date_key:
    :return:
    """
    #for i in values:
    #    print("{}-{}-{}".format(i[0], i[1], key_inside))
    for i in range(len(values) - 1):
        if values[i][0] != values[i+1][0]:
            print("Change {},{}-{},{},from {} to {}".format(garage_id, date, time, key_inside,
                                                            values[i][1], values[i+1][1]))




def measure_accuracy(dicitonary, garage_id, date, time):
    # for key in dictionary.keys():
    #     print(key)
    #     print(dictionary[key])
    #     return
    keys =  sorted([datetime.datetime.strptime(key, date_format) for key in dictionary.keys()])
    key_formatted = [str(time_now.year)+"-"+str(time_now.month) + "-" + str(time_now.day) \
                       + "-" + str(time_now.hour) + "-" + str(time_now.minute) for time_now in keys]

    keys_inside = dictionary[list(dictionary.keys())[0]].keys()

    for key_inside in keys_inside:
        values = []
        for date_key in key_formatted:
            # print("{}, {}, {}".format(key_inside, key, dictionary[key][key_inside][0:10]))
            # get the starting index of the day given
            index  = (datetime.datetime.strptime(date, "%Y-%m-%d") -
                      datetime.datetime.strptime("2018-04-01", "%Y-%m-%d")).days * 24 +\
                     int(time)
            values.append((dictionary[date_key][key_inside][index], date_key))
        print_changes(garage_id, values, key_inside, date, time)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage {} garage_id date<yyyy-mm-dd> time".format(sys.argv[0]))
        sys.exit(0)

    dictionary = make_dictionary(sys.argv[1], sys.argv[2], sys.argv[3])
    measure_accuracy(dictionary, sys.argv[1], sys.argv[2], sys.argv[3])