"""
This file detects if there is any pattern in the receipt of delayed transactions for a given date
and time.  
It prints out how much fraction of the sginals beign compared are highly similar (>90% match)
"""
import sys
import os
import datetime
import json
import numpy as np
from itertools import combinations

groups = ["occupancy", "entries", "exits"]
date_format = "%Y-%m-%d-%H-%M"

def filter_names(file_names, date, time):
    filtered = []
    for f in file_names:
        date_f = f.split("_")[2]
        if datetime.datetime.strptime(date_f, date_format) >= datetime.datetime.strptime(date+"-"+time+"-00", date_format):
            filtered.append(f)

    return filtered

def make_dict_from_json(info):
    dictionary = {}
    json_info = json.loads(info)
    for item in json_info['value']:
        # check if value contains anything
        group = item.get("group").encode('ascii', 'ignore')
        dictionary[group] = item.get("value")
    return dictionary


def make_dictionary(garage_id, date, time):
    """
    makes a dictionary of downloaded data for files with date >= given date
    Key ofnthe dictionary is date+time (375245_exits_2018-4-22-12-10)
    :param garage_id:
    :param date:
    :param time:
    :return:
    """
    file_names = [fn for fn in os.listdir("./data") if garage_id in fn and sys.argv[3] in fn]

    # filter based on date
    filtered_file_names = filter_names(file_names, date, time)

    dictionary = {}
    for file in filtered_file_names:
        # print(file)
        with open("./data/"+file) as f:
            content = f.readlines()
            dictionary[file.split("_")[2]] = make_dict_from_json(content[0])

    return dictionary

def calculate_changes(garage_id, values,  key_inside, date, time):
    """
    Measures changes over time and reports the places when there is a change
    :param values:
    :param key_inside:
    :param date:
    :param time:
    :param date_key:
    :return:
    """
    # for i in values:
    #     print("{}-{}-{}".format(i[0], i[1], key_inside))

    #list of tuples
    changes=[]
    for i in range(len(values)):
        # if values[0][0] != values[i][0]:
        #     changes.append(1)
        # else:
        #     # no changes
        #     changes.append(0)
        changes.append(abs(values[i][0] - values[0][0]))

    return changes




def calculate_change_signal(dictionary, garage_id, date, time):

    #sorting the times
    keys =  sorted([datetime.datetime.strptime(key, date_format) for key in dictionary.keys()])

    #reformatting back to string after sorting
    key_formatted = [str(time_now.year)+"-"+str(time_now.month) + "-" + str(time_now.day) \
                       + "-" + str(time_now.hour) + "-" + str(time_now.minute) for time_now in keys]

    keys_inside = dictionary[list(dictionary.keys())[0]].keys()

    # holds the data for all the changes acoss days
    # [[transient, [change values]], [contract, [change values]].....]
    all_changes =[]

    for key_inside in keys_inside:
        values = []
        for date_key in key_formatted:
            # print("{}, {}, {}".format(key_inside, key, dictionary[key][key_inside][0:10]))
            # get the starting index of the day given
            index  = (datetime.datetime.strptime(date, "%Y-%m-%d") -
                      datetime.datetime.strptime("2018-04-01", "%Y-%m-%d")).days * 24 +\
                     int(time)
            values.append((dictionary[date_key][key_inside][index], date_key))

        all_changes.append([key_inside, calculate_changes(garage_id, values, key_inside, date, time)])

    return all_changes

def mean_absolute_percentage_error(y_true, y_pred):
    """
    Calculate SMAPE, returns the accuracy
    :param y_true: numpy array
    :param y_pred: numpy array
    :return: float
    """
    # flattening the arrays and rounding them
    # transform 0 for negative predictions
    # print("{}--->{}".format(y_true, y_pred))
    y_true = np.around(np.ravel(y_true))
    y_pred = np.around(np.ravel(y_pred))
    y_pred[y_pred < 0] = 0

    sum_t = 0

    for i in range(len(y_true)):
        if y_true[i] == 0 and y_pred[i] == 0:
            continue
        else:
            sum_t = sum_t + (np.abs(y_pred[i] - y_true[i]) / (y_pred[i] + y_true[i]))

    error = (sum_t / len(y_true)) * 100
    accuracy = 100 - error
    return accuracy if accuracy > 0 else 0

def cluster_signals(changes_signals):
    similarity_values = []

    indices = list(range(len(changes_signals)))

    # number of true similars, ignoring mostly zero-valued lists
    true_similar = 0
    all_simlarity = 0

    for comb in combinations(indices, 2):
        # print(comb)
        all_simlarity += 1
        accuracy = mean_absolute_percentage_error(changes_signals[comb[0]][1],
                                                                changes_signals[comb[1]][1])
        similarity_values.append(accuracy)

        if accuracy >= 90:
            # check if mostly zeros
            if changes_signals[comb[0]][1].count(0) < len(changes_signals[comb[0]][1])/2:
                # print("{}-->{}".format(changes_signals[comb[0]],
                #                                                 changes_signals[comb[1]]))
                true_similar += 1

    # how much fraction is high similar
    return (true_similar/all_simlarity) * 100

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print("Usage {} date<yyyy-mm-dd> time occ/ent/ext".format(sys.argv[0]))
        sys.exit(0)

    garage_dictionaries =[]
    with open("garages") as f:
        garages = f.readlines()
    garages = [f.strip() for f in garages]

    for g in garages:
        garage_dictionaries.append(make_dictionary(g, sys.argv[1], sys.argv[2]))


    # list to store all the change time series for all garages
    changes_signals = []

    for index in range(len(garage_dictionaries)):
        changes_single_garage = calculate_change_signal(garage_dictionaries[index], garages[index], sys.argv[1], sys.argv[2])
        for c in changes_single_garage:
            changes_signals.append(c)

    # print(changes_signals)

    similairties = cluster_signals(changes_signals)
    # similairties.sort()

    # for i in similairties:
    #     print(i)
    print(similairties)