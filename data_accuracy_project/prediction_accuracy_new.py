"""
This file is a new estimation algorithm that estimates the real time metric by taking the
average of that metric in the historical time, taking into account weekdays, holidays ets.
It prints the accuracy after the prediciton.
"""
from __future__ import division
import numpy as np
# import pandas as pd
# import matplotlib.pyplot as plt
import sys
import os
import os.path
import json
import random
import math

import datetime
import subprocess
import threading
import multiprocessing

home_dimension = ""
date_format = "%Y-%m-%d"
days_in_the_past = 5
holidays_json_file = "./holidays.json"
holidays = []

garage_name = ""



def set_home_dimension(garage_id):
    """
    set the garage home dimension to make the url

    :param garage_id: str
    :return: None
    """

    global home_dimension

    garage_metadata = json.loads(get_garage_metadata(garage_id))

    # print(garage_metadata)
    if garage_metadata is None:
        home_dimension = ""
    else:
        # extract the home dimension
        home_dimension = garage_metadata["homeDimension"].replace(" ", "+")


def get_garage_metadata(garage_id):
    """
    Get the garage metadata and return

    :param garage_id: str
    :return: str
    """
    # treat special garages
    if garage_id == '552026':
        garage_url = 'unico__g552026'
    elif garage_id == '516873':
        garage_url = 'premier__sondock'
    elif garage_id == '780376':
        garage_url = '780376'
    else:
        garage_url = 'c__g' + garage_id
    url = "https://my.smarking.net/api/users/v1/garages/" + garage_url
    garage_metadata = get_json_info_using_curl(url)
    return garage_metadata



def get_json_info_using_curl(url):
    """
    Download json string using curl

    :param url: str
    :return: str
    """
    token = str(os.environ['Bearer'])
    url_str = 'curl -s -i "' + url + '" -H "Authorization: Bearer ' + token + '"'
    output = subprocess.check_output(url_str, shell=True)
    res = output.decode("utf-8").splitlines()
    if len(res) < 10:
        print("bad result from url", url)
        sys.exit(0)
    return output.decode("utf-8").splitlines()[9]


def get_info_dict_from_all(dict_all, index):
    """
    This function takes the whole downloaded json object,
    extracts the information for a particular day by 'index'
    and returns a dictionary, each key represents each category
    (Contract/ transient etc)

    :param dict_all: str
    :param index: int
    :return:
    """

    dictionary = dict()
    info = json.loads(dict_all)

    for item in info['value']:
        # check if value contains anything
        group = item.get("group").encode('ascii', 'ignore')
        starting_index = index * 24 * 7
        dictionary[group] = np.array(item.get("value"))[starting_index:(starting_index + 24)]
    return dictionary

def get_info_dict(str_info):
    """
    This function creates a dictionary out of JSON object.
    keys are groups (Contract/ Transient) etc.

    :param str str_info: Input json string
    :return: dict
    """
    dictionary = dict()
    info = json.loads(str_info)

    for item in info['value']:
        # check if value contains anything
        group = item.get("group").encode('ascii', 'ignore')
        dictionary[group] = np.array(item.get("value"))
    return dictionary

def read_holidays_json():
    """
    Creates a list of holiday dates

    :return: list[str]
    """
    global holidays

    with open(holidays_json_file, 'r') as f:
        info = json.load(f)
        for i in info:
            date = i['start'][0:4] + "-" + i['start'][4:6] + "-" + i['start'][6:8]
            holidays.append(date)
    return holidays


def get_data(garage_id, date, time, metric):
    """
    This function generates a dictionary for each group for the given
    metric.  key is the group (contract, transient) and values are
    the values until the given time of the day

    :param garage_id: str
    :param date: str
    :param time: str
    :param metric: str
    :return: dict
    """
    global days_in_the_past

    # get data for the past days_in_the_past days
    date_given = datetime.datetime.strptime(date, date_format)
    start_date = date_given - datetime.timedelta(days_in_the_past)

    # calculate number of data_points(hours) to download
    hours = days_in_the_past * 24 + 1 + int(time)
    day_str = str(start_date.year) + "-" + str(start_date.month).zfill(2) \
              + "-" + str(start_date.day).zfill(2)

    url = "https://my.smarking.net/api/ds/v3/garages/" \
          + garage_id + "/past/" + metric + "/from/" + day_str + \
          "T00:00:00/" + str(hours) + "/1h?gb=" + home_dimension

    return get_info_dict(get_json_info_using_curl(url))




def get_weekend_indices(date):
    """
    Get indices of weekends in the past so that
    their data can be extracted from the whole blob
    of downloaded data

    :param str date: date
    :return: list
    """

    global days_in_the_past
    weekend_indices = []
    date_temp = datetime.datetime.strptime(date, date_format)

    for i in range(1, days_in_the_past):
        if (date_temp - datetime.timedelta(i)).weekday() in [5, 6]:
            weekend_indices.append(i)
    return weekend_indices


def get_holiday_indices(date):
    """
        Get indices of holidays in the past so that
        their data can be extracted from the whole blob
        of downloaded data

        :param str date: date
        :return: list
    """

    global days_in_the_past, holidays
    holiday_indices = []
    date_temp = datetime.datetime.strptime(date, date_format)

    for i in range(1, days_in_the_past):
        date_str = str((date_temp - datetime.timedelta(i)).year) + "-"\
                   + str((date_temp - datetime.timedelta(i)).month).zfill(2) + "-"\
                   + str((date_temp - datetime.timedelta(i)).day).zfill(2)
        if date_str in holidays:
            holiday_indices.append(i)

    return holiday_indices


def modify_holidays(data_dict, date, hour):
    """
        This function takes a historical data dictionary and filters
        the dictionary to keep only holiday data
    """

    holiday_indices_in_history = get_holiday_indices(date)

    # modify the dictionary with only holiday data
    for key in data_dict.keys():
        temp_values = []
        for jj in reversed(holiday_indices_in_history):
            for k in data_dict[key]\
                    [(days_in_the_past - jj) * 24:((days_in_the_past - jj) * 24) + 24]:
                temp_values.append(k)
        # add data for the current day
        for k in data_dict[key]\
                    [days_in_the_past * 24:(days_in_the_past * 24) + int(hour) + 1]:
            temp_values.append(k)
        data_dict[key] = temp_values



def modify_weekends(data_dict, date, hour):
    """
    This function takes a historical data dictionary and filters
    the dictionary to keep only weekend data

    :param data_dict: dict
    :param date: date
    :return: None
    """

    weekend_indices_in_history = get_weekend_indices(date)

    # modify the dictionary with only weekend data
    for key in data_dict.keys():
        temp_values = []
        for jj in reversed(weekend_indices_in_history):
            for k in data_dict[key]\
                    [(days_in_the_past - jj) * 24:((days_in_the_past - jj) * 24) + 24]:
                temp_values.append(k)
        # add data for the current day
        for k in data_dict[key]\
                [days_in_the_past * 24:(days_in_the_past * 24) + int(hour) + 1]:
            temp_values.append(k)
        data_dict[key] = temp_values


def print_accuracy(array, time):
    # print(len(array))
    # print(array)
    # get historical data
    hist=[]
    for i in range(math.floor(len(array) / 24)):
        hist.append(array[i*24+int(time)])

    # print(hist)
    predicted = np.average(np.array(hist))

    actual = array[-1]
    # print(predicted, actual)

    if actual == 0:
        accuracy = 100 - (abs(actual - predicted)*100)
    else:
        accuracy = 100 - ((abs(actual - predicted) / actual) * 100)

    if accuracy < 0:
        return 0
    else:
        return accuracy


def prediction(garage_id, date, time, metric):
    global previous_days
    set_home_dimension(garage_id)
    # set_home_dimension(garage_id)
    # # get the date prev_days_for_training ago
    # start_date = date - datetime.timedelta(days=7 * (previous_days + 1))
    #
    # start_date = str(start_date.year) + "-" + str(start_date.month) + \
    #              "-" + str(start_date.day)
    #
    # url = "https://my.smarking.net/api/ds/v3/garages/" \
    #       + garage_id + "/past/" + "occupancy" + "/from/" + start_date + \
    #       "T00:00:00/" + str(7 * previous_days * 24 + (time + 1)) + "/1h?gb=" + home_dimension
    # json_dict_all = get_json_info_using_curl(url)
    #
    # dictionaries = []
    # for rr in range(previous_days):
    #     dictionaries.append(get_info_dict_from_all(json_dict_all, rr))
    # return dictionaries

    short_term_data_dictionary = get_data(garage_id, date, time, metric)

    global holidays
    holidays = read_holidays_json()

    # treat weekends specially
    day_of_the_week = datetime.datetime.strptime(date, date_format).weekday()
    if day_of_the_week in [5, 6]:
        modify_weekends(short_term_data_dictionary, date, time)
    # treat holidays specially, if a holiday is a weekend,
    # being weekend is given priority
    elif date in holidays:
        modify_holidays(short_term_data_dictionary, date, time)

    for key in short_term_data_dictionary.keys():
        #print(key, print_accuracy(short_term_data_dictionary[key], time))
        print("{},{},{},{}".format(time,print_accuracy(short_term_data_dictionary[key], time),garage_id,key))




if __name__ == '__main__':
    if len(sys.argv) < 4:
        print("Usage: python {} garage_id date time <occupancy/entries/exits>".format(sys.argv[0]))
        sys.exit()

    prediction(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])