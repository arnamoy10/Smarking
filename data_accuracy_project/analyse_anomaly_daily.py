import sys
import numpy as np
import datetime

histogram = np.zeros(7)
date_format = "%Y-%m-%d-%H-%M"


def generate_hist(garage_id, file):
    global histogram
    with open(file) as f:
        lines = f.readlines()

    for line in lines:
        # Change 512044,2018-04-11-11,b'PayByPhone',from 2018-4-11-13-15 to 2018-4-11-14-16
        tokens = line.split(',')
        date_from = datetime.datetime.strptime(tokens[1]+"-00", date_format)
        date_to = datetime.datetime.strptime(tokens[3].split(' ')[3].strip(), date_format)
        histogram[(date_to - date_from).days] += 1

    for i in histogram:
        print(i)



if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage {} garage_id results_file".format(sys.argv[0]))
    generate_hist(sys.argv[1], sys.argv[2])