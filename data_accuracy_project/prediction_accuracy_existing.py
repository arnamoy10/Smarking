"""
This file prints the prediction accuracy at a given time for the existing smarking algorithm"""
import sys
import os
import datetime
import json


date_format = "%Y-%m-%d-%H-%M"

def filter_names(file_names, date, time):
    filtered = []
    for f in file_names:
        date_f = f.split("_")[2]
        if datetime.datetime.strptime(date_f, date_format) >= datetime.datetime.strptime(date+"-"+time+"-00", date_format):
            filtered.append(f)

    return filtered

def make_dict_from_json(info):
    dictionary = {}
    json_info = json.loads(info)
    for item in json_info['value']:
        # check if value contains anything
        group = item.get("group").encode('ascii', 'ignore')
        dictionary[group] = item.get("value")
    return dictionary


def make_dictionary(garage_id, date, time):
    """
    makes a dictionary of downloaded data for files with date >= given date
    Key ofnthe dictionary is date+time (375245_exits_2018-4-22-12-10)
    :param garage_id:
    :param date:
    :param time:
    :return:
    """
    file_names = [fn for fn in os.listdir("./data") if garage_id in fn and sys.argv[4] in fn]

    # filter based on date
    filtered_file_names = filter_names(file_names, date, time)

    # print(filtered_file_names)

    dictionary = {}
    for file in filtered_file_names:
        # print(file)
        with open("./data/"+file) as f:
            content = f.readlines()
            dictionary[file.split("_")[2]] = make_dict_from_json(content[0])

    return dictionary


def pred_accuracy(values):
    """
    Measures changes over time and reports the places when there is a change
    :param values:
    :return:
    """
    # for i in values:
    #     print("{}-{}-{}".format(i[0], i[1], key_inside))

    #list of tuples
    # print(values)
    if len(values) == 0:
        return None

    # return abs(values[-1] - values[0])

    if values[-1] == 0:
        accuracy = 100 - (abs(values[-1] - values[0])*100)
    else:
        accuracy = 100 - ((abs(values[-1] - values[0]) / values[-1]) * 100)

    if accuracy < 0:
        return 0
    else:
        return accuracy




def calculate_accuracies(dictionary, garage_id, date, time):

    #sorting the times
    keys =  sorted([datetime.datetime.strptime(key, date_format) for key in dictionary.keys()])

    #reformatting back to string after sorting
    key_formatted = [str(time_now.year)+"-"+str(time_now.month) + "-" + str(time_now.day) \
                       + "-" + str(time_now.hour) + "-" + str(time_now.minute) for time_now in keys]

    keys_inside = dictionary[list(dictionary.keys())[0]].keys()

    # holds the data for all the changes acoss days
    # [[transient, [change values]], [contract, [change values]].....]
    all_changes =[]

    for key_inside in keys_inside:
        values = []
        for date_key in key_formatted:
            # print("{}, {}, {}".format(key_inside, key, dictionary[key][key_inside][0:10]))
            # get the starting index of the day given
            index  = (datetime.datetime.strptime(date, "%Y-%m-%d") -
                      datetime.datetime.strptime("2018-04-01", "%Y-%m-%d")).days * 24 +\
                     int(time)
            values.append(dictionary[date_key][key_inside][index])

        if pred_accuracy(values) is not None:
            print("{},{},{},{}".format(time,pred_accuracy(values),garage_id,key_inside))
        #if pred_accuracy(values) is None:
        #    print("{}".format(garage_id))


if __name__ == '__main__':
    if len(sys.argv) < 5:
        print("Usage {} garage date<yyyy-mm-dd> time occ/ent/ext".format(sys.argv[0]))
        sys.exit(0)

    garage_dictionaries =[]
    garages = [sys.argv[1]]

    # print(garages)

    for g in garages:
        garage_dictionaries.append(make_dictionary(g, sys.argv[2], sys.argv[3]))


    # print(garage_dictionaries)

    # list to store all the change time series for all garages
    changes_signals = []

    for index in range(len(garage_dictionaries)):
        calculate_accuracies(garage_dictionaries[index], garages[index], sys.argv[2], sys.argv[3])