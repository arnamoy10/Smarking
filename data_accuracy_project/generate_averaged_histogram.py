import sys
import numpy as np
num_bins = 7


def generate_hist(file):
    with open(file) as f:
        lines = f.readlines()

    num_garages = len(lines) / 7

    avg_hist = np.zeros(7)

    for i in range(int(num_garages)):
        for j in range(7):
            avg_hist[j] += float(lines[i * 7 + j].strip())

    for j in range(7):
        avg_hist[j] /= 7

    for ii in avg_hist:
        print(ii)


if __name__ == '__main__':
    generate_hist(sys.argv[1])