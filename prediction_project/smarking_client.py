import os, sys
from rnn_tf import *

if __name__ == '__main__':

    # change the authentication token accordingly
    if 'Bearer' not in os.environ:
        print("Please set the Bearer environment variable")
        sys.exit(0)
    bearer = "Bearer " + str(os.environ['Bearer'])
    # headers = {"Authorization": bearer}

    if len(sys.argv) < 4:
        print("Usage {} <garage_id> <date (yyyy-mm-dd)> <time>".format(sys.argv[0]))
        sys.exit(0)

    # (1) When no data is present for a day, generate a model and generate predicted data
    # (2) when predicted data is present, genrate scaled data from that time on till the
    # end of the day

    # check if data is present
    directory_path = sys.argv[1] + "/" + sys.argv[2]

    if os.path.isdir(directory_path):
        # if predicted data is already present, generate scaled data based on current day
        generate_scaled_short_and_long_term_data(sys.argv[1], sys.argv[2], sys.argv[3])

    else:
        # if not, build model and store data
        generate_short_and_long_term_data(sys.argv[1], sys.argv[2])
