"""
This file takes a comma separated list of accuracy results
as an argument and generates a bar chart of accuracies
separated by dimensions (Contract/ Transient etc)

"""

import sys
import matplotlib.pyplot as plt
import csv
import numpy as np

if len(sys.argv) < 3:
    print("Usage python3 {} <results_file1> <results_file2>".format(sys.argv[0]))


#dicitionary to hold accuracies for all dimesions
#{str:List[float]}
dimension_dict={}

with open(sys.argv[1]) as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    for row in csvReader:
        dimension = row[3]
        if dimension in dimension_dict:
            dimension_dict[dimension].append(float(row[4]))
        else:
            #create new key value in dictionary
            dimension_dict[dimension] = []
            dimension_dict[dimension].append(float(row[4]))

#add
dimension_dict_new={}

with open(sys.argv[2]) as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    for row in csvReader:
        dimension = row[2]
        if dimension in dimension_dict_new:
            dimension_dict_new[dimension].append(float(row[3]))
        else:
            #create new key value in dictionary
            dimension_dict_new[dimension] = []
            dimension_dict_new[dimension].append(float(row[3]))

keys=[]
averages_old=[]
averages_new=[]

std_old=[]
std_new=[]

keys_a = set(dimension_dict_new.keys())
keys_b = set(dimension_dict.keys())
common_keys = keys_a & keys_b

for key in common_keys:
    #print('{},{},{}'.format(key, np.mean(dimension_dict[key]), np.std(dimension_dict[key])))
    keys.append(key)
    averages_old.append(np.mean(dimension_dict[key]))
    std_old.append(np.std(dimension_dict[key]))

    averages_new.append(np.mean(dimension_dict_new[key]))
    std_new.append(np.std(dimension_dict_new[key]))

#print for excel
for i in np.arange(len(std_new)):
    print("{},{},{}".format(averages_old[i], averages_new[i], keys[i]))

width = 0.35
ind = np.arange(len(keys))

plt.figure(figsize=(20,20))

fig, ax = plt.subplots(figsize=(20,20))

bar_old = ax.bar(ind, averages_old, width, color='r')
bar_new = ax.bar(ind+width, averages_new, width, color='g')

ax.legend((bar_old[0], bar_new[0]), ('Existing', 'New'))


ax.set_xticks(ind + width / 2)
ax.set_xticklabels(keys, rotation=90)

#plt.xlabel('Dimensions')
#plt.title('Dimension Accuracies')
#plt.ylabel('Avg. %Accuracy')

plt.savefig(sys.argv[1]+'_bar.png')



'''

keys=[]
averages=[]
for key in dimension_dict:
    #print('{},{},{}'.format(key, np.mean(dimension_dict[key]), np.std(dimension_dict[key])))
    keys.append(key)
    averages.append(np.mean(dimension_dict[key]))


plt.figure(figsize=(20,20))
plt.xticks(rotation=90)
plt.bar(keys, averages)
plt.xlabel('Dimensions')
plt.title('Dimension Accuracies')
plt.ylabel('Avg. %Accuracy')

plt.savefig(sys.argv[1]+'_bar.png')
