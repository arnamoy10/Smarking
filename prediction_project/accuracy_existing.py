"""
Analyse accuracy from reading downloaded files and downloading current data
for Smarking algos
"""

from __future__ import division
import requests
from datetime import datetime
import sys
import json
import numpy as np
import subprocess
import os, random
import re

def get_json_info_using_curl(url):
    #print(url)
    token=str(os.environ['Bearer'])
    output = subprocess.check_output('curl -s -i "'+ url +
                                     '" -H "Authorization: Bearer'
                                     + token +'"', shell=True)
    return output.decode("utf-8").splitlines()[9]


def get_json_info(url):
    # get the response using the url
    response = requests.get(url, headers=headers)
    content = response.text

    # see if content was received.  If nothing  received, exit
    if content == "":
        print("No content received")
        return None

    # we have collected all the data
    # each data point is for an hour in a given day
    try:
        garage_info = json.loads(content)
    except ValueError:
        print("No JSON Object received for occupancy, please try again.")
        return None
    # print (garage_info)
    return garage_info


def get_garage_metadata(garage_id):
    # treat special garages
    if garage_id == '552026':
        garage_url = 'unico__g552026'
    elif garage_id == '516873':
        garage_url = 'premier__sondock'
    elif garage_id == '780376':
        garage_url = '780376'
    else:
        garage_url = 'c__g' + garage_id
    url = "https://my.smarking.net/api/users/v1/garages/" + garage_url
    garage_metadata = get_json_info(url)
    return garage_metadata


def get_info_dict(str_info):
    # print(info)
    dictionary = dict()
    info = json.loads(str_info)
    # print(info)
    for item in info['value']:
        # check if value contains anything
        # print item
        # print 'Here**************\n'
        group = item.get("group")
        # print group
        # print np.array(item.get("value"))
        dictionary[group] = np.array(item.get("value"))

    return dictionary


def get_info_dict_from_file(info):
    # print info
    dictionary = dict()
    for item in info["groups"]:
        # check if value contains anything

        group = str(item.get("group"))

        dictionary[group] = np.array(item.get("value"))

    return dictionary


def get_accuracy(original, predicted):
    # calculate the number of elements that fall within 5%
    # of the original array
    # print(original)
    # print(predicted)
    num_within_range = 0
    for i in np.arange(len(original)):
        if (original[i] * 0.90 <= predicted[i] <= original[i] * 1.1):
            num_within_range = num_within_range + 1
    return (num_within_range / len(original)) * 100


def directional_symmetry(y_true, y_pred):
    d_i = []
    for i in range(0, len(y_true) - 1):
        if (y_true[i+1] - y_true[i])(y_pred[i+1] - y_pred[i]) >= 0:
            d_i.append(1)
        else:
            d_i.append(0)
    return 100 / (len(d_i) - 1) * sum(d_i)

def mean_absolute_percentage_error(y_true, y_pred):
    indices = np.where(y_true == 0)[0]

    y_t = [element for i, element in enumerate(y_true) if i not in indices]
    y_p = [element for i, element in enumerate(y_pred) if i not in indices]
    error = 100 - (np.mean(np.abs((np.array(y_t) - np.array(y_p)) / np.array(y_t))) * 100)
    return error if error > 0 else 0


def calculate_and_print_accuracy(type_a):

    file_path = "./" + sys.argv[2] + "/" + sys.argv[1] \
                + "##" + sys.argv[2] + "T" + hour \
                + ":00:00##" +type_a

    data = json.load(open(file_path))

    dict_x = get_info_dict_from_file(data)

    # getting the data 2 days from now in future
    starting = 24 + (24 - int(hour))

    date = "{}-{}-{}".format(sys.argv[2].split('-')[0], sys.argv[2].split('-')[1],
                             str(int(sys.argv[2].split('-')[2]) + 2).zfill(2))

    garage_metadata = get_garage_metadata(sys.argv[1])
    home_dimension = ""
    if garage_metadata is None:
        print("No garage metadata received")
        sys.exit(0)
    else:
        # extract the home dimension
        home_dimension = garage_metadata["homeDimension"].replace(" ", "+")

    url = "https://my.smarking.net/api/ds/v3/garages/" + sys.argv[1]\
          + "/past/" + type_a + "/from/" + date +\
          "T00:00:00/24/1h?gb=" + home_dimension
    # print url
    info = get_json_info_using_curl(url)

    if info is None:
        print
        "No valid JSON data received for {}".format(sys.argv[1])
        sys.exit(0)
    dict = get_info_dict(info)

    # print dict_x
    # print "**********************"
    # print dict

    keys_a = set(dict.keys())
    keys_b = set(dict_x.keys())
    common_keys = keys_a & keys_b

    for key in common_keys:
        actual = np.array(dict[key])
        predicted = dict_x[key][starting:starting + 24]
        # print repr(actual)
        # print '**********'
        # print repr(predicted)
        print '{},{},{},{},{},{}'.format(type_a, sys.argv[1], \
                             sys.argv[2], key, \
                             mean_absolute_percentage_error(actual, predicted),
                                      directional_symmetry(actual, predicted))



if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage {} garage_id yyyy-mm-dd")

    if 'Bearer' not in os.environ:
        print("Please set the Bearer environment variable")
        sys.exit(0)
    bearer = "Bearer " + str(os.environ['Bearer'])
    headers = {"Authorization": bearer}

    # pick an hour from the available files
    match = re.search(r'(\d+:\d+:\d+)', random.choice \
        (os.listdir("./" + sys.argv[2])))
    hour = match.group(1).split(':')[0]

    for i in ['occupancies', 'entries', 'exits']:
        calculate_and_print_accuracy(i)
