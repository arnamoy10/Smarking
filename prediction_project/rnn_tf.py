from __future__ import division
import numpy as np
# import pandas as pd
import tensorflow as tf
# import matplotlib.pyplot as plt
import sys
import os
import os.path
import json
import random

import re
import requests
import datetime
import subprocess
import threading
import multiprocessing

# look at past "n" weeks to get training data for the specific day
# of the week
previous_days_for_training = 20
date_format = "%Y-%m-%d"
holidays_json_file = "./holidays.json"
home_dimension = ""

groups = ["occupancy", "entries", "exits"]


# groups = ["occupancy"]


def make_url(garage_id, start_date, type_a):
    """
    Makes a URL and returns it

    :param garage_id: str
    :param start_date: str
    :param type_a: str
    :return: str
    """
    start_date = str(start_date.year) + "-" + str(start_date.month) + \
                 "-" + str(start_date.day)

    return "https://my.smarking.net/api/ds/v3/garages/" \
           + garage_id + "/past/" + type_a + "/from/" + start_date + \
           "T00:00:00/24/1h?gb=" + home_dimension


def get_json_info_using_curl(url):
    """
    Download json string using curl

    :param url: str
    :return: str
    """
    token = str(os.environ['Bearer'])
    url_str = 'curl -s -i "' + url + '" -H "Authorization: Bearer ' + token + '"'
    output = subprocess.check_output(url_str, shell=True)
    res = output.decode("utf-8").splitlines()
    if len(res) < 10:
        print("bad result from url", url)
        sys.exit(0)
    return output.decode("utf-8").splitlines()[9]


def get_garage_metadata(garage_id):
    """
    Get the garage metadata and return

    :param garage_id: str
    :return: str
    """
    # treat special garages
    if garage_id == '552026':
        garage_url = 'unico__g552026'
    elif garage_id == '516873':
        garage_url = 'premier__sondock'
    elif garage_id == '780376':
        garage_url = '780376'
    else:
        garage_url = 'c__g' + garage_id
    url = "https://my.smarking.net/api/users/v1/garages/" + garage_url
    garage_metadata = get_json_info_using_curl(url)
    return garage_metadata


def get_info_dict(str_info):
    """
    This function creates a dictionary out of JSON object
    keys are Contract/ Transient etc.

    :param str_info: str
    :return: dict
    """
    dictionary = dict()
    info = json.loads(str_info)

    for item in info['value']:
        # check if value contains anything
        group = item.get("group").encode('ascii', 'ignore')
        dictionary[group] = np.array(item.get("value"))
    return dictionary


def get_info_dict_from_all(dict_all, index):
    """
    This function takes the whole downloaded json object,
    extracts the information for a particular day by 'index'
    and returns a dictionary, each key represents each category
    (Contract/ transient etc)

    :param dict_all: str
    :param index: int
    :return:
    """

    dictionary = dict()
    info = json.loads(dict_all)

    for item in info['value']:
        # check if value contains anything
        group = item.get("group").encode('ascii', 'ignore')
        starting_index = index * 24 * 7
        dictionary[group] = np.array(item.get("value"))[starting_index:(starting_index + 24)]
    return dictionary


def get_dictionaries(garage_id, start_date, type_a):
    """
    getting historical data using single curl and creating a list
    of dictionaries, one for each day

    :param garage_id: str
    :param start_date: str
    :param type_a: str
    :return: list
    """
    global previous_days_for_training
    # get the date prev_days_for_training ago
    start_date = start_date - datetime.timedelta(days=7 * (previous_days_for_training + 1))

    start_date = str(start_date.year) + "-" + str(start_date.month) + \
                 "-" + str(start_date.day)

    url = "https://my.smarking.net/api/ds/v3/garages/" \
          + garage_id + "/past/" + type_a + "/from/" + start_date + \
          "T00:00:00/" + str(7 * previous_days_for_training * 24) + "/1h?gb=" + home_dimension
    json_dict_all = get_json_info_using_curl(url)

    dictionaries = []
    for rr in range(previous_days_for_training):
        dictionaries.append(get_info_dict_from_all(json_dict_all, rr))
    return dictionaries


def set_home_dimension(garage_id):
    """
    set the garage home dimension to make the url

    :param garage_id: str
    :return: None
    """

    global home_dimension

    garage_metadata = json.loads(get_garage_metadata(garage_id))

    # print(garage_metadata)
    if garage_metadata is None:
        home_dimension = ""
    else:
        # extract the home dimension
        home_dimension = garage_metadata["homeDimension"].replace(" ", "+")


def get_max_keys(json_dicts):
    """
    Takes a list of dictionaries for days
    and returns the max keys in a given day

    :param json_dicts: list
    :return: list
    """
    global previous_days_for_training

    num_keys = 0
    index_dic = 0
    for iii in range(0, len(json_dicts)):
        if len(json_dicts[iii].keys()) > num_keys:
            num_keys = len(json_dicts[iii].keys())
            index_dic = iii

    return json_dicts[index_dic].keys()


def directional_symmetry(y_true, y_pred):
    """
    Compute Directional Symmetry

    :param y_true: numpy array
    :param y_pred: numpy array
    :return: float
    """
    y_true = np.ravel(y_true)
    y_pred = np.ravel(y_pred)

    d_i = []
    for i in range(0, len(y_true) - 1):
        if (y_true[i + 1] - y_true[i]) * (y_pred[i + 1] - y_pred[i]) >= 0:
            d_i.append(1)
        else:
            d_i.append(0)
    return 100 / (len(d_i) - 1) * sum(d_i)


def mean_absolute_percentage_error(y_true, y_pred):
    """
    Calculate SMAPE, returns the accuracy
    :param y_true: numpy array
    :param y_pred: numpy array
    :return: float
    """
    # flattening the arrays and rounding them
    # transform 0 for negative predictions
    y_true = np.around(np.ravel(y_true))
    y_pred = np.around(np.ravel(y_pred))
    y_pred[y_pred < 0] = 0

    sum_t = 0

    for i in range(len(y_true)):
        if y_true[i] == 0 and y_pred[i] == 0:
            continue
        else:
            sum_t = sum_t + (np.abs(y_pred[i] - y_true[i]) / (y_pred[i] + y_true[i]))

    error = (sum_t / len(y_true)) * 100
    accuracy = 100 - error
    return accuracy if accuracy > 0 else 0


def test_data(series, forecast, num_periods):
    """
    Formats test_data for RNN and returns formatted data

    :param series: list
    :param forecast: int
    :param num_periods: int
    :return: list, list
    """
    test_x_setup = series[-(num_periods + forecast):]
    test_x = test_x_setup[:num_periods].reshape(-1, num_periods, 1)
    test_y = series[-num_periods:].reshape(-1, num_periods, 1)
    return test_x, test_y


def prepare_rnn_network():
    """
    Prepares the tensors for the RNN network
    :return: X, y, optimizer, outputs
    """
    tf.reset_default_graph()  # We didn't have any previous graph objects running, but this would reset the graphs

    num_periods = 24  # number of periods per vector we are using to predict one period ahead
    inputs = 1  # number of vectors submitted
    hidden = 100  # number of neurons we will recursively work through, can be changed to improve accuracy
    output = 1  # number of output vectors

    X = tf.placeholder(tf.float32, [None, num_periods, inputs])  # create variable objects
    y = tf.placeholder(tf.float32, [None, num_periods, output])

    basic_cell = tf.contrib.rnn.BasicRNNCell(num_units=hidden, activation=tf.nn.relu)  # create our RNN object
    rnn_output, states = tf.nn.dynamic_rnn(basic_cell, X, dtype=tf.float32)  # choose dynamic over static

    learning_rate = 0.001  # small learning rate so we don't overshoot the minimum

    stacked_rnn_output = tf.reshape(rnn_output, [-1, hidden])  # change the form into a tensor
    stacked_outputs = tf.layers.dense(stacked_rnn_output, output)  # specify the type of layer (dense)
    outputs = tf.reshape(stacked_outputs, [-1, num_periods, output])  # shape of results

    loss = tf.reduce_sum(tf.square(outputs - y))  # define the cost function which evaluates the quality of our model
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)  # gradient descent method
    training_op = optimizer.minimize(loss)  # train the result of the application of the cost_function

    return X, y, training_op, outputs


def new_rnn(garage_id, date, type_a, key, ts, test1):
    """
    The RNN algorithm

    :param garage_id: str
    :param date: str
    :param type_a: str group (occupancy/ entries/ exits)
    :param key: str (category e.g contracts/ transients etc)
    :param ts: list training data
    :param test1: list (prediction takes this as input to generate the output)
    :return: list
    """

    temp = np.array(ts)

    # have to do it because total number of values in
    # training data should not be a multiple of
    # num_periods/ period of the signal
    timeseries = temp[0:len(temp) - 1]
    num_periods = 24
    f_horizon = 1  # forecast horizon, one period into the future

    x_data = timeseries[:(len(timeseries) - (len(timeseries) % num_periods))]
    x_batches = x_data.reshape(-1, num_periods, 1)

    y_data = timeseries[1:(len(timeseries) - (len(timeseries) % num_periods)) + f_horizon]
    y_batches = y_data.reshape(-1, num_periods, 1)

    x_test, y_test = test_data(test1, f_horizon, num_periods)

    x_t, y, training_op, outputs = prepare_rnn_network()
    init = tf.global_variables_initializer()  # initialize all the variables

    saver = tf.train.Saver()

    epochs = 1000  # number of iterations or training cycles, includes both the FeedFoward and Backpropogation

    model_file = "./" + garage_id + "/" + date + "/" + type_a + "_" + key.decode('ascii') + ".model"
    if not os.path.exists("./" + garage_id):
        os.makedirs("./" + garage_id)

    with tf.Session() as sess:
        init.run()
        for ep in range(epochs):
            sess.run(training_op, feed_dict={x_t: x_batches, y: y_batches})

        predictions = []
        # make 5 consecutive predictions
        y_pred = sess.run(outputs, feed_dict={x_t: x_test})
        predictions.append(y_pred[0])
        for i in range(5):
            x_test, y_test = test_data(y_pred, f_horizon, num_periods)
            y_pred = sess.run(outputs, feed_dict={x_t: x_test})[0]
            predictions.append(y_pred)
        saver.save(sess, model_file)

    return predictions


def train_and_save(garage_id, type_a, date):
    """
    This function generates RNN model and saves predicted data.
    For each date given as parameter, it
       i)   downloads historical data
       ii)  trains a model
       iii) generates predicted data for the same day of the week for next few weeks

    :param garage_id: str
    :param type_a: str
    :param date: str
    :return: None
    """

    s_date = datetime.datetime.strptime(date, date_format)

    # version that does not use multiple curls
    # get a list of dictionaries, each item for a day
    # each dictionary has the key as Contract/Transient and as value the historical data
    json_dictionaries = get_dictionaries(garage_id, s_date, type_a)

    # get the max keys that are present among training days
    # we will ignore the dictionaries that does not have
    # all the keys
    keys = get_max_keys(json_dictionaries)

    # check for each category (Contract, Transient etc.)
    for key in keys:
        # train the rnn
        train_data, test_data = get_training_test_data(json_dictionaries, key)

        date1 = datetime.datetime.strptime(date, date_format)

        # generate predicted data for the same day of the week for the next num_weeks
        predicted = new_rnn(garage_id, str(date1.year) + "-" +
                            str(date1.month).zfill(2) + "-" +
                            str(date1.day).zfill(2),
                            type_a, key, train_data, test_data)

        # save predicted data, the result in the previous step
        # contains predictions for multiple weeks in future
        # for the same day of the week
        num_weeks = 5
        for rng in range(num_weeks):
            date_temp = datetime.datetime.strptime(date, date_format) \
                        + datetime.timedelta(days=rng * 7)

            # create directory if does not exist
            dir_path = garage_id + "/" + str(date_temp.year) + "-" + \
                       str(date_temp.month).zfill(2) + "-" + \
                       str(date_temp.day).zfill(2)
            if not os.path.isdir(dir_path):
                os.makedirs(dir_path)

            temp = predicted[rng]
            temp[temp < 0] = 0
            # save file
            np.savetxt(dir_path + "/" + type_a + "_" + key.decode('ascii'), np.around(temp))


def process_group(group, date, garage_id):
    """
    This function generate a model and predicted data for >1 month starting the date given.
    It loops through 7 days starting from today and then
        for each day, it calls train_and_save() that generates predicted data
        for the same day of the week for the next 5 weeks

    :param group: str
    :param date: str
    :param garage_id: str
    :return: None
    """

    # how many consecutive days to loop for in future
    num_jobs = 7

    # Parallel version
    jobs = []
    for d in range(num_jobs):
        date_temp = datetime.datetime.strptime(date, date_format) \
                    + datetime.timedelta(days=d)
        day_str = str(date_temp.year) + "-" + str(date_temp.month).zfill(2) \
                  + "-" + str(date_temp.day).zfill(2)
        p = multiprocessing.Process(target=train_and_save, args=(garage_id, group, day_str))
        jobs.append(p)
        p.start()
    for jj in jobs:
        jj.join()
    '''
    # serial version
    for d in range(num_jobs):
        # increment the day
        date_temp = datetime.datetime.strptime(date, date_format) \
                    + datetime.timedelta(days=d)
        day_str = str(date_temp.year) + "-" + str(date_temp.month).zfill(2) \
                  + "-" + str(date_temp.day).zfill(2)
        train_and_save(garage_id, group, day_str)
    '''


def read_holidays_json():
    """
    Creates a list of holiday dates in the past 3 years
    :return: list
    """
    holidays = []
    current_year = datetime.datetime.now().year
    current_month = datetime.datetime.now().month
    with open(holidays_json_file, 'r') as f:
        info = json.load(f)
        for i in info:
            # look until the past three years
            if int(i['start'][0:4]) > (current_year - 3) and int(i['start'][0:4]) <= current_year:
                # look until the previous month of current month
                if int(i['start'][0:4]) == current_year:
                    if int(i['start'][4:6]) < current_month:
                        date = i['start'][0:4] + "-" + i['start'][4:6] + "-" + i['start'][6:8]
                        holidays.append(date)
                else:
                    date = i['start'][0:4] + "-" + i['start'][4:6] + "-" + i['start'][6:8]
                    holidays.append(date)
    return holidays


def new_rnn_holidays(garage_id, type_a, key, ts, test1):
    """
    Build a RNN model for holidays and saves into a file for that garage,
    one per group (Occ, entries, exits) per category (Contract, Transient)

    :param garage_id: str
    :param type_a: str
    :param key: str
    :param ts: list
    :param test1: list
    :return: None
    """

    temp = np.array(ts)

    # have to do it because total number of values in
    # training data should not be a multiple of
    # num_periods/ period of the signal
    timeseries = temp[0:len(temp) - 1]
    num_periods = 24
    f_horizon = 1  # forecast horizon, one period into the future

    x_data = timeseries[:(len(timeseries) - (len(timeseries) % num_periods))]
    x_batches = x_data.reshape(-1, num_periods, 1)

    y_data = timeseries[1:(len(timeseries) - (len(timeseries) % num_periods)) + f_horizon]
    y_batches = y_data.reshape(-1, num_periods, 1)

    x_test, y_test = test_data(test1, f_horizon, num_periods)

    x_t, y, training_op, outputs = prepare_rnn_network()
    init = tf.global_variables_initializer()  # initialize all the variables

    # check if a model was built already
    model_file = "./" + garage_id + "/holidays_" + type_a + "_" + key.decode('ascii') + ".txt"
    if not os.path.exists("./" + garage_id):
        os.makedirs("./" + garage_id)

    epochs = 1000  # number of iterations or training cycles, includes both the FeedFoward and Backpropogation

    with tf.Session() as sess:
        init.run()
        for ep in range(epochs):
            sess.run(training_op, feed_dict={x_t: x_batches, y: y_batches})

        y_pred = sess.run(outputs, feed_dict={x_t: x_test})
        # save the holiday predictions
        np.savetxt(model_file, np.around(y_pred[0]))


def get_training_test_data(dicts, key):
    """
    Generates training and test data
    :param dicts:
    :param key:
    :return:
    """
    features = []
    good_indices = []
    for j in np.arange(len(dicts)):
        # check if key is present in the dictionary
        if key in dicts[j].keys():
            good_indices.append(j)
            for ii in dicts[j][key]:
                features.append(ii)

    # pick test data from a random day
    test_data_r = dicts[good_indices[random.randint(0, len(good_indices) - 1)]][key]
    return features, test_data_r


def generate_holidays_data(garage_id):
    """
    Calls helper function to build model and generate predicted data
    for a given garage.

    :param garage_id: str
    :return: None
    """
    # get a list of holidays
    holidays = read_holidays_json()

    global groups

    for g in groups:
        holiday_dictionaries = []
        for i in holidays:
            url = make_url(garage_id, datetime.datetime.strptime(i, date_format), g)
            holiday_dictionaries.append(get_info_dict(get_json_info_using_curl(url)))

        # getting indices from good data and choosing a random test day
        keys = get_max_keys(holiday_dictionaries)

        # check for each categories (Contract/ Transient etc.)
        for key in keys:
            # get training and test data, both timeseries
            training_data, test_data = get_training_test_data(holiday_dictionaries, key)

            # generate holiday model and predicted data
            new_rnn_holidays(garage_id, g, key, training_data, test_data)


def replace_holiday_data(garage_id, date):
    """
    Replace predicted data for holidays in future with holiday data

    :param garage_id: str
    :param date: str
    :return: None
    """
    # get holidays in the next 1.5 months, because we
    # generate predicted data for that time range
    holidays = read_holidays_json()

    today = datetime.datetime.strptime(date, date_format)
    margin = datetime.timedelta(days=35)

    for i in holidays:
        if today <= datetime.datetime.strptime(i, date_format) <= today + margin:
            date_temp = datetime.datetime.strptime(i, date_format)
            dir_path = garage_id + "/" + str(date_temp.year) + "-" + \
                       str(date_temp.month).zfill(2) + "-" + \
                       str(date_temp.day).zfill(2) + "/"
            if not os.path.isdir(dir_path):
                continue
            # do for each group
            global groups
            # groups = ["occupancy", 'entries', 'exits']
            for g in groups:
                files = [f for f in os.listdir(dir_path) if re.match(r'{0}_[a-zA-Z]'.format(g), f)]
                prediction_files = [f for f in files if not '.' in f]
                # extract the group from dimension
                for f in prediction_files:
                    grp1 = f.split("_")[1]
                    holiday_data = np.loadtxt(garage_id + "/holidays_" + g + "_" + grp1 + ".txt")
                    np.savetxt(dir_path + f, np.around(holiday_data))


def generate_short_and_long_term_data(garage_id, date):
    """
    Generate a new model and new predicted data for the next month or so
    starting from the "date".  Considers holidays as well.

    :param garage_id: str
    :param date: str
    :return: None
    """

    set_home_dimension(garage_id)

    global groups

    num_threads1 = len(groups)

    # parallel version
    jobs_1 = []
    for grp in range(num_threads1):
        p1 = multiprocessing.Process(target=process_group, args=(groups[grp], date, garage_id))
        jobs_1.append(p1)
        p1.start()
        # for i in ["occupancy"]:
        # do it for the current date and the next n dates
    for jj1 in jobs_1:
        jj1.join()
    '''
    # serial version
    for grp in range(num_threads1):
        process_group(groups[grp], date, garage_id)
    '''
    # deal with holidays
    generate_holidays_data(garage_id)
    # if there are upcoming holidays, replace their predicted data with holiday data
    replace_holiday_data(garage_id, date)


def get_short_term(garage_id, date, time, group):
    """
    This function generates a dictionary for each category for the given
    group.  key is the category (contract, transient) and values are
    the values until time starting from the day

    :param garage_id: str
    :param date: str
    :param time: str
    :param group: str
    :return: dictionary
    """
    url = "https://my.smarking.net/api/ds/v3/garages/" \
          + garage_id + "/past/" + group + "/from/" + date + \
          "T00:00:00/" + str(int(time) - 1).zfill(2) + "/1h?gb=" + home_dimension
    return get_info_dict(get_json_info_using_curl(url))


def get_from_file(garage_id, date, group, key):
    """
    Read predicted data from file to numpy array for the given garage->date->group->key
    Reads for the whole day

    :param garage_id: str
    :param date: str
    :param group: str
    :param key: str
    :return: numpy array
    """

    path = "./" + str(garage_id) + "/" + str(date) + "/" + str(group) + "_" + str(key.decode('ascii'))

    if not os.path.exists(path):
        print("No file present for the garage {} on date {}, {}_{}"
              .format(garage_id, date, group, key))
        sys.exit(0)
    return np.loadtxt(path)


def generate_scaled_data(short_term_data, predicted_data, time):
    """
    Generate scaled data and return scaled numpy array

    :param short_term_data: numpy array
    :param predicted_data: numpy array
    :param time: str
    :return: numpy array
    """
    # get indices of nonzero values of predicted data
    temp_predicted = np.array(predicted_data)[0:int(time) - 1]
    ind = np.where(temp_predicted != 0)

    scale = np.average(short_term_data[ind] / temp_predicted[ind])
    temp_predicted = temp_predicted * scale
    temp_predicted[temp_predicted < 0] = 0
    return temp_predicted


def generate_scaled_short_and_long_term_data(garage_id, date, time):
    """
    This function generates scaled data according the data seen for the day so far


    :param garage_id: str
    :param date: str
    :param time: str
    :return: None
    """

    set_home_dimension(garage_id)

    global groups

    for grp in groups:
        # get data for the day from beginning till this hour (given by time)
        short_term_data_dictionary = get_short_term(garage_id, date, time, grp)

        for key in short_term_data_dictionary.keys():
            features = []
            for ii in short_term_data_dictionary[key]:
                features.append(ii)

            # get stored predicted data for the whole day
            predicted_data = get_from_file(garage_id, date, grp, key)

            # generate scaled data using predicted data and data so far
            scaled_data = generate_scaled_data(short_term_data_dictionary[key], predicted_data, time)

            # save generated scaled data
            path = str(garage_id) + "/" + str(date) + "/" + str(grp) + "_" + str(key.decode('ascii'))

            np.savetxt(path, np.around(scaled_data))
